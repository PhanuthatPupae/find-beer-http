package beef

import (
	"beef/controller"
)

func init() {
	methodRoutes[ROUTE_GET]["/beef/summary"] = controller.GetBeefSummaryByType

}
