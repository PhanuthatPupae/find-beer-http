package service_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"

	"beef/logger"
	service "beef/services"
)

func TestCleanText(t *testing.T) {
	t.Log("Start TestCleanText")

	viper.AddConfigPath("../config")
	viper.SetConfigName("config")

	// Read Config
	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "unable to read config: %v\n", err)
		os.Exit(1)
	}

	logger.InitLogger()

	s := service.New(uuid.NewString())
	output, err := s.GetBeefSummaryByType(service.GetBeefSummaryByTypeInput{})
	if err != nil {
		t.Errorf("failed to GetBeefSummaryByType cuz: %s", err)
		assert.NotNil(t, err)
	}

	assert.NotEmpty(t, output.Text)
	assert.NotEmpty(t, output.Beef)
	assert.NotEmpty(t, output.Words)

}
