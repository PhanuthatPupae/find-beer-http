package service

import (
	"context"
	"strings"

	"beef/external/beef"
)

type GetBeefSummaryByTypeInput struct {
}

type GetBeefSummaryByTypeOutput struct {
	Text  string      `json:"text"`
	Words []string    `json:"words"`
	Beef  interface{} `json:"beef"`
}

/*
https://gosamples.dev/remove-duplicate-spaces/
*/
func (service Service) GetBeefSummaryByType(input GetBeefSummaryByTypeInput) (GetBeefSummaryByTypeOutput, error) {
	service.Logger.Infof("start GetBeefSummaryByType")
	output := GetBeefSummaryByTypeOutput{}

	beefService := beef.New(context.Background(), service.RequestId)
	beef, err := beefService.GetBeefText(beef.BeefInput{})
	if err != nil {
		return output, err
	}

	// init text to lowwer case and remove , . white space and multiples apce
	initBeefText := cleanText(beef.Text)

	wordList := strings.Split(initBeefText, " ")
	mapWordSum := make(map[string]int)
	for _, w := range wordList {
		mapWordSum[w] += 1
	}

	output.Text = initBeefText
	output.Words = wordList
	output.Beef = mapWordSum
	service.Logger.Infof("GetBeefSummaryByType Done")
	return output, nil
}

func cleanText(str string) string {
	if str == "" {
		return ""
	}

	initBeefText := strings.ToLower(str)
	initBeefText = strings.ReplaceAll(initBeefText, ".", "")
	initBeefText = strings.ReplaceAll(initBeefText, ",", "")
	initBeefText = strings.ReplaceAll(initBeefText, "\n", "")
	initBeefText = strings.Join(strings.Fields(initBeefText), " ")

	return initBeefText
}
