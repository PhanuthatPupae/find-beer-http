package custom_error

type CustomError struct {
	Code    int32  `json:"code"`
	Message string `json:"message"`
}

func (c CustomError) Error() string {
	return c.Message
}
