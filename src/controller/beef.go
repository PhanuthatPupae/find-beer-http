package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"beef/interface/http/response"
	service "beef/services"
)

func GetBeefSummaryByType(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to GetBeefSummaryByType")
	responseOutput := response.ResponseOutput{}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	input := service.GetBeefSummaryByTypeInput{}

	// if err := context.ShouldBindJSON(&input); err != nil {
	// 	apiLogger.Errorf("could not bind json body to GetBeefSummaryByType for validation : %s", err.Error())
	// 	responseOutput.Message = err.Error()
	// 	context.JSON(http.StatusBadRequest, responseOutput)
	// 	return
	// }

	serviceObj := service.New(requestIdStr)
	responseData, err := serviceObj.GetBeefSummaryByType(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = responseData
	context.JSON(http.StatusOK, responseOutput)
}
