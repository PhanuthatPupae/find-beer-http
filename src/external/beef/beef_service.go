package beef

import (
	"context"

	"github.com/spf13/viper"
	"go.uber.org/zap"

	"beef/logger"
)

type Beef struct {
	Context   context.Context
	RequestId string
	Logger    *zap.SugaredLogger
	Url       string
}

func New(context context.Context, requestId string) Beef {

	beef := Beef{
		RequestId: requestId,
	}

	beef.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "beef",
	)

	beef.Context = context
	beef.Url = viper.GetString("Beef.Url")
	return beef
}
