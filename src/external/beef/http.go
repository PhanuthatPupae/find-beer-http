package beef

import (
	"bytes"
	"context"
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"time"
)

func (beef *Beef) RequestHttp(context context.Context, url string, input []byte, method string, token string) (int, string, []byte, error) {
	beef.Logger.Infof("Requesting http to url: %s method: %s body: %s", url, method, string(input))

	request, err := http.NewRequest(method, url, bytes.NewBuffer(input))
	if err != nil {
		beef.Logger.Errorf("Create request failed to url: %s method: %s cuz: %s", url, method, err.Error())
		return 0, "", nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("request_id", beef.RequestId)
	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	client := &http.Client{
		Transport: transport,
		Timeout:   10 * time.Second,
	}

	response, err := client.Do(request)
	if err != nil {
		beef.Logger.Errorf("Request failed to url: %s method: %s cuz: %s", url, method, err.Error())
		return 0, "", nil, err
	}
	defer response.Body.Close()
	contenType := response.Header.Get("Content-Type")

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		beef.Logger.Errorf("Read response body failed to url: %s method: %s cuz: %s", url, method, err.Error())
		return 0, "", nil, err
	}

	beef.Logger.Debugf("Response http to url: %s method: %s body: %s", url, method, string(responseBody))
	return response.StatusCode, contenType, responseBody, nil
}
