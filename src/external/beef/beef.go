package beef

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

type BeefInput struct {
}

type BeefOutput struct {
	Text string
}

func (beef *Beef) GetBeefText(input BeefInput) (*BeefOutput, error) {

	body, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}

	url := beef.Url
	status, _, res, err := beef.RequestHttp(context.Background(), url, body, "GET", "")
	if status != http.StatusOK {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	output := BeefOutput{
		Text: string(res),
	}

	fmt.Println("output ======>", output)

	// err = json.Unmarshal(res, &output)
	// if err != nil {
	// 	return nil, err
	// }
	return &output, nil
}
