#!/bin/sh

docker exec -i beef-pg psql -U postgres -c "drop database if exists beef" &&
docker exec -i beef-pg psql -U postgres -c "create database beef"

